package ir.maktab.finalproject;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.websocket.DeploymentException;
import javax.websocket.EncodeException;
import javax.websocket.Session;

import org.glassfish.tyrus.client.ClientManager;

import ir.maktab.finalproject.client.ClientEndpoint;
import ir.maktab.finalproject.entities.Message;
import ir.maktab.finalproject.entities.User;
import ir.maktab.finalproject.util.JsonUtil;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	public static Stage mainStage;
	public static List<Scene> list = new ArrayList<Scene>();
	protected static Session session;
	public static User thisUser;

	protected static final String ROOT = new File("").getAbsolutePath();
	public static final String SERVER = "ws://localhost:8025/maktabsharif/chat";

	public static void main(String[] args) throws Exception {
		webSocketConnection();
		launch(args);
		session.close();
	}

	private static void webSocketConnection() throws URISyntaxException, DeploymentException, IOException {
		ClientManager client = ClientManager.createClient();
		session = client.connectToServer(ClientEndpoint.class, new URI(SERVER));
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		mainStage = primaryStage;

		list.add(new Scene(
				(Parent) new FXMLLoader(new File(ROOT + "/src/main/resource/view/LoginPanel.fxml").toURI().toURL())
						.load()));

		list.add(new Scene(
				(Parent) new FXMLLoader(new File(ROOT + "/src/main/resource/view/UsersViewPanel.fxml").toURI().toURL())
						.load()));

		list.add(new Scene(
				(Parent) new FXMLLoader(new File(ROOT + "/src/main/resource/view/ChatroomPanel.fxml").toURI().toURL())
						.load()));

		setScene(SceneKey.LOGIN);
		mainStage.show();
	}

	public static void setScene(SceneKey key, User... user) {
		if (key == SceneKey.LOGIN) {
			Platform.runLater(() -> {
				mainStage.setScene(list.get(0));
			});
		} else if (key == SceneKey.USERS) {
			Platform.runLater(() -> {
				mainStage.setScene(list.get(1));
				mainStage.setTitle(user[0].getName());
				thisUser = user[0];
			});
		} else if (key == SceneKey.CHATROOM) {
			Platform.runLater(() -> {
				mainStage.setScene(list.get(2));
			});
		}
	}

	public static void checkUser(User user) throws IOException, EncodeException {
		Message message = new Message();
		message.setSender(user);
		session.getBasicRemote().sendText(JsonUtil.formatMessageObject(message));
	}

}