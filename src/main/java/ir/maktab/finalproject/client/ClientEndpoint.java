package ir.maktab.finalproject.client;

import static java.lang.String.format;

import java.text.SimpleDateFormat;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import ir.maktab.finalproject.Main;
import ir.maktab.finalproject.SceneKey;
import ir.maktab.finalproject.entities.Message;
import ir.maktab.finalproject.model.MessageDecoder;
import ir.maktab.finalproject.model.MessageEncoder;

@javax.websocket.ClientEndpoint(encoders = MessageEncoder.class, decoders = MessageDecoder.class)
public class ClientEndpoint {

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat();

	@OnOpen
	public void onOpen(Session session) {
		System.out.println(format("Connection established. session id: %s", session.getId()));
	}

	@OnMessage
	public void onMessage(Message message) {
		if (message.getReceiver().getId() == 0) {
			if (message.getSender().getId() != 0)
			 Main.setScene(SceneKey.USERS, message.getSender());
			else
				Main.setScene(SceneKey.LOGIN);
		} else {
			System.out.println(format("[%s:%s] %s", simpleDateFormat.format(message.getDate()),
					message.getSender().getUserName(), message.getContent()));
		}
	}
}
