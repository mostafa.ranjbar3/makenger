package ir.maktab.finalproject.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class DAO<T> {
	public Connection connection;
	public PreparedStatement statement;

	private String connectionURL = "jdbc:mysql://localhost/finalproject?user=root&password=";
	{
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(connectionURL);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public abstract void create(T entity) throws SQLException;

	public abstract T read(T entity) throws SQLException;

	public abstract void update(T entity) throws SQLException;

	public abstract void delete(T entity) throws SQLException;
}
