package ir.maktab.finalproject.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import ir.maktab.finalproject.entities.User;
import ir.maktab.finalproject.rowmapper.LoginRowMapper;
import ir.maktab.finalproject.rowmapper.UserRowMapper;

public class UserDAO extends DAO<User> {
	private static UserDAO instance;

	public static UserDAO getInstance() {
		if (instance == null) {
			instance = new UserDAO();
		}
		return instance;
	}

	private UserDAO() {
	}

	@Override
	public void create(User entity) throws SQLException {
		String query = "INSERT INTO users(username, password, name) VALUES(?, ?, ?)";
		statement = connection.prepareStatement(query);
		statement.setString(1, entity.getUserName());
		statement.setString(2, entity.getPassword());
		statement.setString(3, entity.getName());
		statement.executeUpdate();
	}

	@Override
	public User read(User entity) throws SQLException {
		String query = "SELECT * FROM users WHERE username = ? and password = ?";
		statement = connection.prepareStatement(query);
		statement.setString(1, entity.getUserName());
		statement.setString(2, entity.getPassword());
		ResultSet result = statement.executeQuery();
		return new UserRowMapper().mapping(result);
	}

	@Override
	public void update(User entity) throws SQLException {
		String query = "UPDATE users SET password=?, name=? WHERE username=?";
		statement = connection.prepareStatement(query);
		statement.setString(3, entity.getUserName());
		statement.setString(1, entity.getPassword());
		statement.setString(2, entity.getName());
		statement.executeUpdate();
	}

	@Override
	public void delete(User entity) throws SQLException {
		String query = "DELETE FROM users WHERE username=?, password=?";
		statement = connection.prepareStatement(query);
		statement.setString(1, entity.getUserName());
		statement.setString(2, entity.getPassword());
		statement.executeUpdate();
	}

	public User login(User user) throws SQLException {
		try {
			User newUser = read(user);
			if (newUser.getId() != 0) {
				String query = "INSERT INTO login(user_id, login_date) VALUES(?, ?)";
				statement = connection.prepareStatement(query);
				statement.setInt(1, newUser.getId());
				statement.setTimestamp(2, new Timestamp(new Date().getTime()));
				statement.executeUpdate();
			}
			return newUser;
		} catch (SQLException sql) {
			return user;
		}
	}

	public void logout(User user) throws SQLException {
		String query = "UPDATE login SET logout_date=? WHERE id=?";
		statement = connection.prepareStatement(query);
		statement.setTimestamp(1, new Timestamp(new Date().getTime()));
		statement.setInt(2, readLast(user.getId()));
		statement.executeUpdate();
	}

	private int readLast(int id) throws SQLException {
		String query = "SELECT * FROM login WHERE user_id=? ORDER BY 1 DESC LIMIT 1";
		PreparedStatement innerStatement = connection.prepareStatement(query);
		innerStatement.setInt(1, id);
		ResultSet result = innerStatement.executeQuery();
		return new LoginRowMapper().mapping(result);
	}
}
