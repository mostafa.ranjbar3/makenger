package ir.maktab.finalproject.entities;

import java.util.Date;

public class Message extends Entity {
	private User sender = new User();
	private User receiver = new User();
	private String content = "";
	private Date date = new Date();
	private boolean visibleForMe;
	private boolean deleted;
	private boolean sent;
	private boolean received;
	private boolean seen;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isVisibleForMe() {
		return visibleForMe;
	}

	public void setVisibleForMe(boolean visibleForMe) {
		this.visibleForMe = visibleForMe;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

	public boolean isReceived() {
		return received;
	}

	public void setReceived(boolean received) {
		this.received = received;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}

}
