package ir.maktab.finalproject.entities;

public class User extends Entity {
	private String userName;
	private String password;
	private String name;

	public User(String userName) {
		this.userName = userName;
	}

	public User(String username, String password) {
		this(username);
		this.setPassword(password);
	}

	public User() {
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
