package ir.maktab.finalproject.manager;

import java.sql.SQLException;

import ir.maktab.finalproject.dao.DAO;

public class EntityManager<T> {
	private DAO<T> dao;

	public DAO<T> getDao() {
		return dao;
	}

	public void setDao(DAO<T> dao) {
		this.dao = dao;
	}

	public void create(T entity) throws SQLException {
		dao.create(entity);
	}

	public T read(T entity) throws SQLException {
		return dao.read(entity);
	}

	public void update(T entity) throws SQLException {
		dao.update(entity);
	}

	public void delete(T entity) throws SQLException {
		dao.delete(entity);
	}
}
