package ir.maktab.finalproject.manager;

import java.sql.SQLException;

import ir.maktab.finalproject.dao.UserDAO;
import ir.maktab.finalproject.entities.User;

public class UserManager extends EntityManager<User> {
	private static UserManager instance;

	public static UserManager getInstance() {
		if (instance == null) {
			instance = new UserManager();
		}
		return instance;
	}

	private UserManager() {
		setDao(UserDAO.getInstance());
	}

	public User login(User user) throws SQLException {
		return UserDAO.getInstance().login(user);
	}

	public void logout(User user) throws SQLException {
		UserDAO.getInstance().logout(user);
	}
}