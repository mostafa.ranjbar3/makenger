package ir.maktab.finalproject.model;

import java.io.IOException;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import com.fasterxml.jackson.databind.ObjectMapper;

import ir.maktab.finalproject.entities.Message;

public class MessageDecoder implements Decoder.Text<Message> {

	@Override
	public void init(final EndpointConfig config) {
	}

	@Override
	public void destroy() {
	}

	@Override
	public Message decode(final String textMessage) throws DecodeException {
		Message message;
		try {
			message = new ObjectMapper().readValue(textMessage, Message.class);
			return message;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean willDecode(final String s) {
		return true;
	}

}
