package ir.maktab.finalproject.model;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import com.fasterxml.jackson.core.JsonProcessingException;

import ir.maktab.finalproject.entities.Message;
import ir.maktab.finalproject.util.JsonUtil;

public class MessageEncoder implements Encoder.Text<Message> {

	@Override
	public void init(final EndpointConfig config) {
	}

	@Override
	public void destroy() {
	}

	@Override
	public String encode(final Message message) throws EncodeException {
		try {
			return JsonUtil.formatMessageObject(message);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
