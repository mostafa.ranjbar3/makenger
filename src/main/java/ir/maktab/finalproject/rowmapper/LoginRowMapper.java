package ir.maktab.finalproject.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginRowMapper {

	public int mapping(ResultSet result) throws SQLException {
		if (result.next()) {
			return result.getInt(1);
		} else
			return 0;
	}

}
