package ir.maktab.finalproject.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RowMapper<T> {
	T mapping(ResultSet result) throws SQLException;
}
