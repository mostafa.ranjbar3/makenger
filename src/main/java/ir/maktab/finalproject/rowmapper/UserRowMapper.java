package ir.maktab.finalproject.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import ir.maktab.finalproject.entities.User;

public class UserRowMapper implements RowMapper<User> {

	public User mapping(ResultSet result) throws SQLException {
		if (result.next()) {
			User user = new User();
			user.setId(result.getInt(1));
			user.setUserName(result.getString(2));
			user.setPassword(result.getString(3));
			user.setName(result.getString(4));
			return user;
		}
		return null;
	}

}
