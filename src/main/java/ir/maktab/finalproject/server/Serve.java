package ir.maktab.finalproject.server;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/chatroom")
public class Serve {
	private static Set<Session> sessionSet = new HashSet<Session>();

	@OnOpen
	public void onOpen(Session session, EndpointConfig ec) {
		sessionSet.add(session);
	}

	@OnClose
	public void onClose(Session session, CloseReason reason) throws IOException {
		sessionSet.remove(session);
		session.close();
	}

	@OnMessage
	public void receiveMessage(String message, Session session) throws IOException {

		Iterator<Session> it = sessionSet.iterator();
		while (it.hasNext()) {
			Session session1 = it.next();
			for (Session s : session1.getOpenSessions()) {
				s.getBasicRemote().sendText(message);
			}
		}
	}

	@OnError
	public void onError(Throwable t) {
		System.out.println(t);
	}
}
