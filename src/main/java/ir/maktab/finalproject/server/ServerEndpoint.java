package ir.maktab.finalproject.server;

import static java.lang.String.format;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import ir.maktab.finalproject.entities.Message;
import ir.maktab.finalproject.entities.User;
import ir.maktab.finalproject.model.MessageDecoder;
import ir.maktab.finalproject.model.MessageEncoder;
import ir.maktab.finalproject.service.UserService;
import ir.maktab.finalproject.util.JsonUtil;

@javax.websocket.server.ServerEndpoint(value = "/chat", encoders = MessageEncoder.class, decoders = MessageDecoder.class)
public class ServerEndpoint {

	static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());

	@OnOpen
	public void onOpen(Session session) {
		System.out.println(format("%s joined the chat room.", session.getId()));
		peers.add(session);
	}

	@OnMessage
	public void onMessage(Message message, Session session) throws IOException, EncodeException, SQLException {
		if (message.getReceiver().getId() == 0) {
			if (message.getContent().equals("Signin")) {
				Message user = message;
				user.setSender(UserService.getInstance().login(message.getSender()));
				session.getBasicRemote().sendText(JsonUtil.formatMessageObject(user));
			} else if (message.getContent().equals("Signup")) {
				Message user = message;
				UserService.getInstance().create(message.getSender());
				user.setSender(UserService.getInstance().read(user.getSender()));
				session.getBasicRemote().sendText(JsonUtil.formatMessageObject(user));
			} else if (message.getContent().equals("Signout")){
				UserService.getInstance().logout(message.getSender());
			}
		} else {
			System.out.println(format("[%s:%s] %s", session.getId(), message.getDate(), message.getContent()));

			// broadcast the message
			for (Session peer : peers) {
				if (!session.getId().equals(peer.getId())) {
					// do not resend the message to its sender
					peer.getBasicRemote().sendObject(message);
				}
			}
		}
	}

	@OnClose
	public void onClose(Session session) throws IOException, EncodeException {
		System.out.println(format("%s left the chat room.", session.getId()));
		peers.remove(session);
		// notify peers about leaving the chat room
		for (Session peer : peers) {
			Message chatMessage = new Message();
			chatMessage.setSender(new User("Server"));
			chatMessage.setContent(format("%s left the chat room.", (String) session.getUserProperties().get("user")));
			chatMessage.setDate(new Date());
			peer.getBasicRemote().sendObject(chatMessage);
		}
	}

}