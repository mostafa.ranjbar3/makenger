package ir.maktab.finalproject.service;

import java.sql.SQLException;

import ir.maktab.finalproject.manager.EntityManager;

public class EntityService<T> {
	private EntityManager<T> manager;

	public EntityManager<T> getManager() {
		return manager;
	}

	public void setManager(EntityManager<T> manager) {
		this.manager = manager;
	}
	
	public void create(T entity) throws SQLException {
		manager.create(entity);
	}

	public T read(T entity) throws SQLException {
		return manager.read(entity);
	}

	public void update(T entity) throws SQLException {
		manager.update(entity);
	}

	public void delete(T entity) throws SQLException {
		manager.delete(entity);
	}
}
