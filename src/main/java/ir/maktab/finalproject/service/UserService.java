package ir.maktab.finalproject.service;

import java.sql.SQLException;

import ir.maktab.finalproject.entities.User;
import ir.maktab.finalproject.manager.UserManager;

public class UserService extends EntityService<User> {
	private static UserService instance;

	private UserService() {
		setManager(UserManager.getInstance());
	}

	public static UserService getInstance() {
		if (instance == null)
			instance = new UserService();
		return instance;
	}

	public User login(User user) throws SQLException {
		return UserManager.getInstance().login(user);
	}

	public void logout(User user) throws SQLException {
		UserManager.getInstance().logout(user);
	}
}
