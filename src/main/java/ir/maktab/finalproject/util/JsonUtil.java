package ir.maktab.finalproject.util;

import javax.json.Json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ir.maktab.finalproject.entities.Message;

public class JsonUtil {

	public static String formatMessage(String message, String user) {
		return Json.createObjectBuilder().add("message", message).add("sender", user).add("received", "").build()
				.toString();
	}

	public static String formatMessageObject(Message message) throws JsonProcessingException {
		return new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(message);
	}
}
