package ir.maktab.finalproject.view;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;

import ir.maktab.finalproject.Main;
import ir.maktab.finalproject.entities.Message;
import ir.maktab.finalproject.entities.User;
import ir.maktab.finalproject.util.JsonUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class LoginPanelController extends Main {
	public static final String SERVER = "ws://localhost:8025/maktabsharif/chat";
	@FXML
	private TextField username;
	@FXML
	private TextField password;
	@FXML
	private Label error;

	public void loginToMessenger() throws JsonProcessingException, IOException {
		Message message = new Message();
		thisUser = new User(username.getText(), password.getText());
		message.setSender(thisUser);
		message.setContent("Signin");
		session.getBasicRemote().sendText(JsonUtil.formatMessageObject(message));
	}

	public void signupToMessenger() throws JsonProcessingException, IOException {
		Message message = new Message();
		thisUser = new User(username.getText(), password.getText());
		message.setSender(thisUser);
		message.setContent("Signup");
		session.getBasicRemote().sendText(JsonUtil.formatMessageObject(message));
	}
}
