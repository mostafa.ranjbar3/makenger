package ir.maktab.finalproject.view;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;

import ir.maktab.finalproject.Main;
import ir.maktab.finalproject.SceneKey;
import ir.maktab.finalproject.entities.Message;
import ir.maktab.finalproject.util.JsonUtil;

public class UserViewPanelController extends Main {

	public void loadChatroomPanel() {
		Main.setScene(SceneKey.CHATROOM);
	}

	public void loadLoginPanel() throws JsonProcessingException, IOException {
		Message message = new Message();
		message.setSender(thisUser);
		message.setContent("Signout");
		session.getBasicRemote().sendText(JsonUtil.formatMessageObject(message));
		Main.setScene(SceneKey.LOGIN);
	}

}
